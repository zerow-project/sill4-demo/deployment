#/bin/bash

helm repo add tsg https://nexus.dataspac.es/repository/tsg-helm
helm repo update

namespace="ids-connectors"
chart_version="3.2.2-master"

for values in values.*.yaml
do
    name=$(echo $values | cut -d'.' -f 2)

    if [ "$1" = "delete" ]
    then
        helm delete -n $namespace $name
    else
        helm ${1:-upgrade} ${2:---create-namespace --install} -n $namespace $name tsg/tsg-connector --version $chart_version -f $values
        sleep 5
    fi
done
