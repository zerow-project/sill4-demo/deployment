# Deployment SILL4

This is a folder that contains helm chart value files for deploying the SILL4 demo. The secrets should be created for each participant (they have been given out by the DAPS). The `upgrade-connectors.sh` script installs the connector. Basically it just creates commands like the following:

```
helm upgrade --create-namespace --install -n ids-connectors farmer tsg/tsg-connector --version 3.2.2-master -f values.farmer.yaml
```
### Deploy Farmer Secret
```
kubectl create secret generic \
    -n ids-connectors \
    farmer-ids-identity-secret \
    --from-file=ids.crt=./certificates/connectors/Farmer/ids.crt \
    --from-file=ids.key=./certificates/connectors/Farmer/ids.key \
    --from-file=ca.crt=./certificates/ca/cachain.crt
```


### Deploy Mobile-Press Secret
```
kubectl create secret generic \
    -n ids-connectors \
    press-ids-identity-secret \
    --from-file=ids.crt=./certificates/connectors/Mobile-Press/ids.crt \
    --from-file=ids.key=./certificates/connectors/Mobile-Press/ids.key \
    --from-file=ca.crt=./certificates/ca/cachain.crt
```

### Deploy Foodbank Secret
```
kubectl create secret generic \
    -n ids-connectors \
    foodbank-ids-identity-secret \
    --from-file=ids.crt=./certificates/connectors/Foodbank/ids.crt \
    --from-file=ids.key=./certificates/connectors/Foodbank/ids.key \
    --from-file=ca.crt=./certificates/ca/cachain.crt
```

### Connectors
```
cd connectors/sill4
./upgrade-connectors.sh

```