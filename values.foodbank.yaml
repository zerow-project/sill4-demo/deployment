# The `broker` block configures the `broker` dependency, as defined in `Chart.yaml`
coreContainer:
  image: docker.nexus.dataspac.es/core-container:1.1.0
  ingress:
    path: /router(.*)
    rewriteTarget: /router$1
    clusterIssuer: letsencrypt
  secrets:
    idsIdentity:
      name: foodbank-ids-identity-secret
  startupProbe:
    failureThreshold: 6
    periodSeconds: 10

adminUi:
  enabled: true
  image: docker.nexus.dataspac.es/ui/core-container-ui:feature-open-sourcing
  ingress:
    path: /ui(/|$)(.*)
    rewriteTarget: /$2
    clusterIssuer: letsencrypt

deployment:
  pullPolicy: Always

host: foodbank.zerow.dataspac.es

ids:
  info:
    idsid: &idsid urn:zerow:tsg:connectors:FoodBankConnector
    curator: &participant urn:zerow:tsg:Willem
    maintainer: *participant
    titles:
      - Foodbank Connector
    descriptions:
      - ZeroW FoodBank Connector
    accessUrl: https://{{ .Values.host }}/router

  daps:
    url: https://daps.zerow.dataspac.es/v2

  broker:
    id: urn:zerow:tsg:connectors:MetadataBroker
    address: https://broker.zerow.dataspac.es/infrastructure
    autoRegister: true

  routes:
    ingress:
      http:
        - endpoint: router
          dataApp: http://{{ template "tsg-connector.fullname" . }}-openapi-data-app-http:8080/router
          parameters: "&matchOnUriPrefix=true"

  workflow:
    useOrchestration: false

  orchestrationManagerConfig:
    enableKubernetes: false

  security:
    apiKeys:
      - id: default
        key: APIKEY-REDACTED
        roles:
          - DATA_APP
          - PEF_MANAGER
    # API User Configuration
    users:
      - # -- User identifier (also used as username for the user)
        id: REDACTED
        # -- BCrypt encoded password
        password: REDACTED
        # -- User role assignments
        roles:
          - ADMIN
containers:
  - type: data-app
    name: openapi-data-app
    image: docker.nexus.dataspac.es/data-apps/openapi-data-app:2.1.0
    apiKey: APIKEY-REDACTED
    services:
      - port: 8080
        name: http
    config:
      logging:
        level:
          nl.tno: DEBUG
      openApi:
        usePolicyEnforcement: false
        openApiBaseUrl: https://app.swaggerhub.com/apiproxy/registry/WILLEMDATEMA/sill4/
        versions:
          - 0.2.0
        agents:
          - backendUrlMapping:
              0.2.0: http://{{template "tsg-connector.fullname" $ }}-sill4-backend-http:8080/
            id: urn:zerow:foodbank
            title: SILL4 Demo - FoodBank
  - type: helper
    name: sill4-backend
    image: docker.nexus.dataspac.es/zerow/sill4backend:main
    services:
      - port: 8080
        name: http
    environment:
      - name: FORWARD
        value: http://{{ template "tsg-connector.fullname" $ }}-openapi-data-app-http:8080/openapi/0.2.0
      - name: SENDER_ID
        value: urn:zerow:foodbank
      - name: LOGOUT_URL
        value: "https://foodbank-auth.zerow.dataspac.es/logout?rd=https%3A%2F%2Ffoodbank-auth.zerow.dataspac.es%2Fsignin?rd=https%3A%2F%2Ffoodbank.zerow.dataspac.es"
  - type: helper
    name: sill4-foodbank-ui
    image: docker.nexus.dataspac.es/zerow/foodbank-ui:main
    services:
      - port: 80
        name: http
        ingress:
          path: /(.*)
          clusterIssuer: letsencrypt
    environment:
      - name: API_BACKEND
        value: http://{{ template "tsg-connector.fullname" $ }}-sill4-backend-http:8080/
